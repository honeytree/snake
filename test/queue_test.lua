local lust = require"lib.lust"
Object = require"lib.classic"
require"src.util.queue"

local describe, it, expect, before = lust.describe, lust.it, lust.expect, lust.before

describe('Queue', function()
    
  before(function()
    qu = Queue(30)
  end)

  it('should be empty after creation', function()
    expect(qu:empty()).to.be.truthy()
  end)

  it('should error if tried to pop (top) empty queue', function()
    expect(qu:pop()).to.fail()
    expect(qu:top()).to.fail()
  end)

  describe('if pushed a single (not nil) element', function()

    before(function()
      qu:push("")
    end)

    it('should be non empty', function()
      expect(not qu:empty()).to.be.truthy()
    end)

    it('top should deliver the same pushed element', function()
      expect(qu:top()).to.equal("")
    end)

    it('should be empty after one element is popped', function()
      qu:pop()
      expect(qu:empty()).to.be.truthy()
    end)

  end)

  describe('if containing the maximum number (30) of elements', function()

    before(function()
      for i = 1, 30 do
        qu:push(i)
      end
    end)

    it('it should overflow if one more element is pushed', function()
      expect(qu:push(31)).to.fail()
    end)

    describe('and five elements are popped', function()

      before(function()
        for i = 1, 5 do
          qu:pop()
        end
      end)

      it('should be of size 25', function()
        expect(qu.size).to.equal(25)
      end)

      it('should have the 30th element as top', function()
        expect(qu:top()).to.equal(30)
      end)

    end)

    describe('and all elements are popped', function()

      before(function()
        for i = 1, 30 do
          qu:pop()
        end
      end)

      it('should be empty', function()
        expect(qu:empty()).to.be.truthy()
      end)

    end)

    describe('and all elements are popped and pushed again', function()

      before(function()
        for i = 1, 30 do
          qu:push(qu:pop())
        end
      end)

      it('should be non empty', function()
        expect(not qu:empty()).to.be.truthy()
      end)

      it('should contain all elements in order', function()
        for i = 1, 30 do
          expect(qu:pop()).to.equal(i)
        end
      end)

    end)

  describe('if pushed five elements', function()

    before(function()
      for i=1,5 do
        qu:push(i)
      end
    end)

    it('the iterator should return it in order', function()
      local test = {1,2,3,4,5}
      for i in qu:iter() do
        expect(i).to.equal(test[i+1]) -- one based arrays, but queue is zero based
      end
    end)
  end)   

  end)
end)