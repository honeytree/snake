Queue = Object:extend()

function Queue:new(maxsize)

    self.maxsize = maxsize
    self.size = 0
    self.q = {}
    self.head = 0
    self.tail = 0

end

function Queue:empty()

    return self.size == 0

end


function Queue:overflow()
    return self.size ~= 0 and self.head == self.tail
end

function Queue:push(elem)

    if not elem then return end

    if self:overflow() then
        return
    end

    self.q[self.head] = elem
    self.head = math.fmod(self.head + 1, self.maxsize)

    self.size = self.size + 1

end

function Queue:pop()

    if self:empty() then
        return
    end

    local del = self.q[self.tail]
    self.q[self.tail] = nil 
    self.tail = math.fmod(self.tail + 1, self.maxsize)

    self.size = self.size - 1

    return del

end

function Queue:top()

    if self:empty() then
        return
    end
    
    return self.q[math.fmod(self.head + self.maxsize - 1, self.maxsize)]

end

function Queue:iter()

    local index = self.tail
    local overflow = self:overflow()

    return function()
        if index ~= self.head or overflow then
            overflow = false -- bad style
            index = math.fmod(index + 1, self.maxsize)
            return self.q[math.fmod(index + self.maxsize - 1, self.maxsize)]
        end
    end

end