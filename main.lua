function love.load()

    -- Configuration and global variables
    --love.window.setFullscreen(true)
    print("Height", love.graphics.getHeight())
    print("Width", love.graphics.getWidth())
    math.randomseed(os.time())

    apple = love.graphics.newImage("assets/apple.png")
    bg = love.graphics.newImage("assets/bg.png")

    MARGIN = 70
    H = love.graphics.getHeight() - 2 * MARGIN
    W = love.graphics.getWidth() - 2 * MARGIN
    GRIDMAX = 30
    CELLSIZE = H / GRIDMAX
    gamefont = love.graphics.newFont(30)
    gamestate = 1
    lock = false

    btnwidth = W / 5
    btnheight = H / 9
    btnx1 = W / 2 - btnwidth / 2 + MARGIN
    btny1 = H / 4
    hover1 = false

    -- require libraries
    Object = require "lib/classic"
    tick = require "lib/tick"
    get_position = require "src/util/position"
    require "src/util/queue"
    require "src/obj/player"
    require "src/obj/food"

    reset_game()

end

function love.update(dt)
    if gamestate == 1 then
        local x, y = love.mouse.getPosition()

        hover1 =
            x > btnx1 and x < btnx1 + btnwidth and y > btny1 and y < btny1 +
                btnheight
    elseif gamestate == 2 then
        tick.update(dt)
    end
end

function love.draw()

    -- draw game objects
    if gamestate == 1 then

        love.graphics.setColor(1, 1, 1)

        love.graphics.draw(bg)

        if hover1 then
            love.graphics.setColor(1, 1, 0)
        else
            love.graphics.setColor(1, 1, 1)
        end

        -- Draw start button
        love.graphics.rectangle("line", btnx1, btny1, btnwidth, btnheight)

        love.graphics.setNewFont(40)

        local text = love.graphics.newText(love.graphics.getFont(), "START")

        love.graphics.draw(text, btnx1 + (btnwidth - text:getWidth()) / 2,
                           btny1 + (btnheight - text:getHeight()) / 2)
    elseif gamestate == 2 or gamestate == 3 then
        love.graphics.rectangle("line", (W - H) / 2 + MARGIN, MARGIN, H, H)
        player:draw()
        goal1:draw()
        goal2:draw()

        local gameover = love.graphics.newText(gamefont, "Game Over")
        local score = love.graphics.newText(gamefont,
                                            "Score: " .. (player.qu.size - 3))
        local pause = love.graphics.newText(gamefont, "Pause")

        local gw = gameover:getWidth()
        local sw = score:getWidth()
        local pw = pause:getWidth()

        love.graphics.draw(score, W / 2 - sw / 2 + MARGIN, 10)

        if gamestate == 3 then
            love.graphics.draw(pause, W / 2 - pw / 2 + MARGIN, H + MARGIN + 20)
        end

        if player:lost() then
            love.graphics.draw(gameover, W / 2 - gw / 2 + MARGIN, H + MARGIN + 20)
        else
            
        end
    end
end

function love.mousepressed(x, y, button, istouch, presses)
    if gamestate == 1 then
        if hover1 then
            -- love.update = nil
            love.graphics.setColor(1, 1, 1)
            gamestate = 2
        end
    end
end

function love.keypressed(key)

    if key == "p" then
        if gamestate == 2 then
            gamestate = 3
        elseif gamestate == 3 then
            gamestate = 2
        end
    end

    if (gamestate == 2 or gamestate == 3) and key == "q" then
        gamestate = 1
        movement:stop()
        reset_game()
    end
    if gamestate == 2 and
        (key == "right" or key == "left" or key == "up" or key == "down") then
        if (not lock) and (direction ~= "up" or key ~= "down") and
            (direction ~= "down" or key ~= "up") and
            (direction ~= "left" or key ~= "right") and
            (direction ~= "right" or key ~= "left") then
            direction = key
            lock = true
        end
    end
end

function spawn_food()

    local i = math.random(0, GRIDMAX - 1)
    local j = math.random(0, GRIDMAX - 1)

    return Food(i, j)

end

function reset_game()
    player = Player(GRIDMAX / 2, GRIDMAX / 2, GRIDMAX ^ 2)
    goal1 = spawn_food()
    goal2 = spawn_food()
    direction = "right"

    movement = tick.recur(function()

        if player:lost() then
            movement:stop()
        else
            player:update(direction)
        end

        if player.qu:top().i == goal1.i and player.qu:top().j == goal1.j then
            goal1 = spawn_food()
            player.grow = true
        end

        if player.qu:top().i == goal2.i and player.qu:top().j == goal2.j then
            goal2 = spawn_food()
            player.grow = true
        end

    end, 0.1)
end
